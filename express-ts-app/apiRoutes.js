"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const neo4j_driver_1 = __importDefault(require("neo4j-driver"));
const express_graphql_1 = require("express-graphql");
const graphql_1 = require("graphql");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const uri = process.env.NEO4J_URI;
const user = process.env.NEO4J_USER;
const password = process.env.NEO4J_PASSWORD;
if (!uri || !user || !password) {
    console.error('One or more required environment variables are missing.');
    process.exit(1);
}
const driver = neo4j_driver_1.default.driver(uri, neo4j_driver_1.default.auth.basic(user, password));
//const driver: Driver = neo4j.driver('neo4j+s://f348fa67.databases.neo4j.io', neo4j.auth.basic('neo4j', 'sckBIM929jDARY3T5LGzZ-yMaRcUG23_uuqNFL1WvC0'));
//const driver: Driver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD));
const session = driver.session();
const MovieType = new graphql_1.GraphQLObjectType({
    name: 'Movie',
    fields: {
        title: { type: graphql_1.GraphQLString },
        movieId: { type: graphql_1.GraphQLString },
    },
});
const ActorType = new graphql_1.GraphQLObjectType({
    name: 'Actor',
    fields: {
        name: { type: graphql_1.GraphQLString },
        url: { type: graphql_1.GraphQLString },
    },
});
const DirectorType = new graphql_1.GraphQLObjectType({
    name: 'Director',
    fields: {
        name: { type: graphql_1.GraphQLString },
        url: { type: graphql_1.GraphQLString },
    },
});
const MovieWithActorsType = new graphql_1.GraphQLObjectType({
    name: 'MovieWithActors',
    fields: {
        title: { type: graphql_1.GraphQLString },
        poster: { type: graphql_1.GraphQLString },
        url: { type: graphql_1.GraphQLString },
        released: { type: graphql_1.GraphQLString },
        plot: { type: graphql_1.GraphQLString },
        movieId: { type: graphql_1.GraphQLString },
        actors: { type: new graphql_1.GraphQLList(ActorType) },
        directors: { type: new graphql_1.GraphQLList(DirectorType) },
    },
});
const MovieByPersonType = new graphql_1.GraphQLObjectType({
    name: 'MovieByPerson',
    fields: {
        title: { type: graphql_1.GraphQLString },
        url: { type: graphql_1.GraphQLString },
    },
});
const RootQueryType = new graphql_1.GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        movies: {
            type: new graphql_1.GraphQLList(MovieType),
            resolve: (parent, args) => __awaiter(void 0, void 0, void 0, function* () {
                const query = `
          MATCH (n:Movie)
          RETURN n.title AS title, n.movieId AS movieId
        `;
                try {
                    const newSession = driver.session();
                    const result = yield newSession.run(query);
                    const movies = result.records.map((record) => {
                        return record.toObject();
                    });
                    yield newSession.close();
                    return movies;
                }
                catch (error) {
                    throw error;
                }
            }),
        },
        actors: {
            type: new graphql_1.GraphQLList(ActorType),
            resolve: (parent, args) => __awaiter(void 0, void 0, void 0, function* () {
                const query = `
          MATCH (n:Actor)
          RETURN n.name AS name;
        `;
                try {
                    const newSession = driver.session();
                    const result = yield newSession.run(query);
                    const actors = result.records.map((record) => {
                        return record.toObject();
                    });
                    yield newSession.close();
                    return actors;
                }
                catch (error) {
                    throw error;
                }
            }),
        },
        moviesWithActors: {
            type: new graphql_1.GraphQLList(MovieWithActorsType),
            resolve: (parent, args) => __awaiter(void 0, void 0, void 0, function* () {
                const query = `
        MATCH (m:Movie)
        WITH m
        ORDER BY COALESCE(m.imdbRating, 0.0) DESC
        RETURN
          m.title AS title,
          m.poster AS poster
        `;
                try {
                    const result = yield session.run(query);
                    const moviesWithActors = result.records.map((record) => ({
                        title: record.get('title'),
                        poster: record.get('poster'),
                    }));
                    return moviesWithActors;
                }
                catch (error) {
                    throw error;
                }
            }),
        },
        movieDetails: {
            type: MovieWithActorsType,
            args: {
                title: { type: graphql_1.GraphQLString },
            },
            resolve: (parent, args) => __awaiter(void 0, void 0, void 0, function* () {
                const query = `
          MATCH (m:Movie {title: $title})
          OPTIONAL MATCH (m)-[:ACTED_IN]-(a:Actor)
          OPTIONAL MATCH (m)-[:DIRECTED]-(d:Director)
          RETURN
            m.title AS title,
            m.poster AS poster,
            m.url AS url,
            m.released AS released,
            m.plot AS plot,
            m.movieId AS movieId,
            COLLECT(DISTINCT { name: a.name, url: a.url }) AS actors,
            COLLECT(DISTINCT { name: d.name, url: d.url }) AS directors;
        `;
                try {
                    const result = yield session.run(query, { title: args.title });
                    const movieDetails = result.records.map((record) => ({
                        title: record.get('title'),
                        poster: record.get('poster'),
                        url: record.get('url'),
                        released: record.get('released'),
                        plot: record.get('plot'),
                        movieId: record.get('movieId'),
                        actors: record.get('actors').map(actor => ({ name: actor.name, url: actor.url })),
                        directors: record.get('directors').map(director => ({ name: director.name, url: director.url })),
                    }))[0];
                    return movieDetails;
                }
                catch (error) {
                    throw error;
                }
            }),
        },
        moviesByPerson: {
            type: new graphql_1.GraphQLList(MovieByPersonType),
            args: {
                name: { type: graphql_1.GraphQLString },
            },
            resolve: (parent, args) => __awaiter(void 0, void 0, void 0, function* () {
                const personName = args.name;
                const query = `
          MATCH (p:Person {name: $personName})-[:ACTED_IN|DIRECTED]-(m:Movie)
          RETURN m.title AS title, m.url AS url
        `;
                try {
                    const result = yield session.run(query, { personName });
                    const moviesByPerson = result.records.map((record) => ({
                        title: record.get('title'),
                        url: record.get('url'),
                    }));
                    return moviesByPerson;
                }
                catch (error) {
                    throw error;
                }
            }),
        },
    },
});
const schema = new graphql_1.GraphQLSchema({
    query: RootQueryType,
});
const router = express_1.default.Router();
router.use('/graphql', (0, express_graphql_1.graphqlHTTP)({
    schema,
    graphiql: true,
}));
exports.default = router;
