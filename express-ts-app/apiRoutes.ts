import express from 'express';
import { Router } from 'express';
import neo4j, { Driver, Session, Record } from 'neo4j-driver';
import { graphqlHTTP } from 'express-graphql';
import { GraphQLObjectType, GraphQLSchema, GraphQLString, GraphQLList } from 'graphql';
import dotenv from 'dotenv';
dotenv.config();

const uri: string | undefined = process.env.NEO4J_URI;
const user: string | undefined = process.env.NEO4J_USER;
const password: string | undefined = process.env.NEO4J_PASSWORD;

if (!uri || !user || !password) {
  console.error('One or more required environment variables are missing.');
  process.exit(1);
}

const driver: Driver = neo4j.driver(uri, neo4j.auth.basic(user, password));

//const driver: Driver = neo4j.driver('neo4j+s://f348fa67.databases.neo4j.io', neo4j.auth.basic('neo4j', 'sckBIM929jDARY3T5LGzZ-yMaRcUG23_uuqNFL1WvC0'));
//const driver: Driver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD));
const session: Session = driver.session();

const MovieType: GraphQLObjectType = new GraphQLObjectType({
  name: 'Movie',
  fields: {
    title: { type: GraphQLString },
    movieId: { type: GraphQLString },
  },
});

const ActorType: GraphQLObjectType = new GraphQLObjectType({
  name: 'Actor',
  fields: {
    name: { type: GraphQLString },
    url: { type: GraphQLString }, 
  },
});

const DirectorType: GraphQLObjectType = new GraphQLObjectType({
  name: 'Director',
  fields: {
    name: { type: GraphQLString },
    url: { type: GraphQLString }, 
  },
});

const MovieWithActorsType: GraphQLObjectType = new GraphQLObjectType({
  name: 'MovieWithActors',
  fields: {
    title: { type: GraphQLString },
    poster: { type: GraphQLString },
    url: { type: GraphQLString },
    released: { type: GraphQLString },
    plot: { type: GraphQLString },
    movieId: { type: GraphQLString },
    actors: { type: new GraphQLList(ActorType) },
    directors: { type: new GraphQLList(DirectorType) }, 
  },
});

const MovieByPersonType: GraphQLObjectType = new GraphQLObjectType({
  name: 'MovieByPerson',
  fields: {
    title: { type: GraphQLString },
    url: { type: GraphQLString },
  },
});

const RootQueryType: GraphQLObjectType = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    movies: {
      type: new GraphQLList(MovieType),
      resolve: async (parent, args) => {
        const query: string = `
          MATCH (n:Movie)
          RETURN n.title AS title, n.movieId AS movieId
        `;

        try {
          
          const newSession = driver.session();
          
          const result = await newSession.run(query);
          
          const movies = result.records.map((record: Record) => {
            return record.toObject();
          });

         
          await newSession.close();

          return movies;
        } catch (error) {
          throw error;
        }
      },
    },
    actors: {
      type: new GraphQLList(ActorType),
      resolve: async (parent, args) => {
        const query: string = `
          MATCH (n:Actor)
          RETURN n.name AS name;
        `;

        try {
          
          const newSession = driver.session();
          
          const result = await newSession.run(query);
          
          const actors = result.records.map((record: Record) => {
            return record.toObject();
          });

          
          await newSession.close();

          return actors;
        } catch (error) {
          throw error;
        }
      },
    },
    moviesWithActors: {
      type: new GraphQLList(MovieWithActorsType),
      resolve: async (parent, args) => {
        const query: string = `
        MATCH (m:Movie)
        WITH m
        ORDER BY COALESCE(m.imdbRating, 0.0) DESC
        RETURN
          m.title AS title,
          m.poster AS poster
        `;
    
        try {
          const result = await session.run(query);
          const moviesWithActors = result.records.map((record: Record) => ({
            title: record.get('title'),
            poster: record.get('poster'),  
          }));
    
          return moviesWithActors;
        } catch (error) {
          throw error;
        }
      },
    },
    movieDetails: {
      type: MovieWithActorsType, 
      args: {
        title: { type: GraphQLString },
      },
      resolve: async (parent, args) => {
        const query: string = `
          MATCH (m:Movie {title: $title})
          OPTIONAL MATCH (m)-[:ACTED_IN]-(a:Actor)
          OPTIONAL MATCH (m)-[:DIRECTED]-(d:Director)
          RETURN
            m.title AS title,
            m.poster AS poster,
            m.url AS url,
            m.released AS released,
            m.plot AS plot,
            m.movieId AS movieId,
            COLLECT(DISTINCT { name: a.name, url: a.url }) AS actors,
            COLLECT(DISTINCT { name: d.name, url: d.url }) AS directors;
        `;

        try {
          const result = await session.run(query, { title: args.title });
          const movieDetails = result.records.map((record: Record) => ({
            title: record.get('title'),
            poster: record.get('poster'),  
            url: record.get('url'),    
            released: record.get('released'),    
            plot: record.get('plot'),
            movieId: record.get('movieId'),
            actors: (record.get('actors') as { name: string, url: string }[]).map(actor => ({ name: actor.name, url: actor.url })),
            directors: (record.get('directors') as { name: string, url: string }[]).map(director => ({ name: director.name, url: director.url })),  
          }))[0]; 

          return movieDetails;
        } catch (error) {
          throw error;
        }
      },
    },
    moviesByPerson: {
      type: new GraphQLList(MovieByPersonType),
      args: {
        name: { type: GraphQLString },
      },
      resolve: async (parent, args) => {
        const personName = args.name;
        const query = `
          MATCH (p:Person {name: $personName})-[:ACTED_IN|DIRECTED]-(m:Movie)
          RETURN m.title AS title, m.url AS url
        `;

        try {
          const result = await session.run(query, { personName });

          const moviesByPerson = result.records.map((record: Record) => ({
            title: record.get('title'),
            url: record.get('url'),
          }));

          return moviesByPerson;
        } catch (error) {
          throw error;
        }
      },
    },
  },
});



const schema: GraphQLSchema = new GraphQLSchema({
  query: RootQueryType,
});

const router: Router = express.Router();

router.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true,
}));

export default router;
